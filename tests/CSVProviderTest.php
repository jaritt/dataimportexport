<?php
/**
 * Created by PhpStorm.
 * User: felix.welter
 * Date: 20.07.2017
 * Time: 10:19
 */

use Mate\DataImportExport\CSVProvider;
use Orchestra\Testbench\TestCase;

class CSVProviderTest extends TestCase
{
    public function testConfigure()
    {
        $provider = new CSVProvider(["zahl" => "1", "stadt" => "hamburg"]);
        $provider->configure(["zahl" => "2", "auto" => "tesla"]);
        $this->assertEquals(["zahl" => "2", "auto" => "tesla", "stadt" => "hamburg"], $provider->getConfig());
    }

    /**
     * SourceFile Content:
     * h1;h2,leave
     * hallo;welt,me
     * test;now,alone
     */

    public function testCommaHeader()
    {
        // GIVEN
        $provider = new CSVProvider();
        $provider->configure(['sourceFile' => __DIR__ . '/test.csv', 'delimiter' => ',', 'header' => true]);
        $iterator = $provider->getIterator();

        // WHEN
        /**
         * @var $iterator Iterator
         */
        $iterator->rewind();

        // THEN
        $this->assertEquals('hallo;welt', $iterator->current()['h1;h2']);
        $this->assertEquals('me', $iterator->current()['leave']);

        // WHEN
        $iterator->next();

        // THEN
        $this->assertEquals('test;now', $iterator->current()['h1;h2']);
        $this->assertEquals('alone', $iterator->current()['leave']);
    }

    public function testSemicolonHeader()
    {
        // GIVEN
        $provider = new CSVProvider();
        $provider->configure(['sourceFile' => __DIR__ . '/test.csv', 'delimiter' => ';', 'header' => true]);
        $iterator = $provider->getIterator();

        // WHEN
        /**
         * @var $iterator Iterator
         */
        $iterator->rewind();

        // THEN
        $this->assertEquals('hallo', $iterator->current()['h1']);
        $this->assertEquals('welt,me', $iterator->current()['h2,leave']);

        // WHEN
        $iterator->next();

        // THEN
        $this->assertEquals('test', $iterator->current()['h1']);
        $this->assertEquals('now,alone', $iterator->current()['h2,leave']);
    }

    public function testCommaNoHeader()
    {
        // GIVEN
        $provider = new CSVProvider();
        $provider->configure(['sourceFile' => __DIR__ . '/test.csv', 'delimiter' => ',', 'header' => false]);
        $iterator = $provider->getIterator();

        // WHEN
        /**
         * @var $iterator Iterator
         */
        $iterator->rewind();

        // THEN
        $this->assertEquals('h1;h2', $iterator->current()[0]);
        $this->assertEquals('leave', $iterator->current()[1]);

        // WHEN
        $iterator->next();

        // THEN
        $this->assertEquals('hallo;welt', $iterator->current()[0]);
        $this->assertEquals('me', $iterator->current()[1]);
        // WHEN
        $iterator->next();

        // THEN
        $this->assertEquals('test;now', $iterator->current()[0]);
        $this->assertEquals('alone', $iterator->current()[1]);
    }

    public function testSemicolonNoHeader()
    {
        // GIVEN
        $provider = new CSVProvider();
        $provider->configure(['sourceFile' => __DIR__ . '/test.csv', 'delimiter' => ';', 'header' => false]);
        $iterator = $provider->getIterator();

        // WHEN
        /**
         * @var $iterator Iterator
         */
        $iterator->rewind();

        // THEN
        $this->assertEquals('h1', $iterator->current()[0]);
        $this->assertEquals('h2,leave', $iterator->current()[1]);

        // WHEN
        $iterator->next();

        // THEN
        $this->assertEquals('hallo', $iterator->current()[0]);
        $this->assertEquals('welt,me', $iterator->current()[1]);
        // WHEN
        $iterator->next();

        // THEN
        $this->assertEquals('test', $iterator->current()[0]);
        $this->assertEquals('now,alone', $iterator->current()[1]);
    }
}
