<?php
/**
 * Created by PhpStorm.
 * User: felix.welter
 * Date: 20.07.2017
 * Time: 10:39
 */

use Mate\DataImportExport\ExcelProvider;

class ExcelProviderTest extends PHPUnit_Framework_TestCase
{
    public function testConfigure()
    {
        $provider = new ExcelProvider(["zahl" => "1", "stadt" => "hamburg"]);
        $provider->configure(["zahl" => "2", "auto" => "tesla"]);
        $this->assertEquals(["zahl" => "2", "auto" => "tesla", "stadt" => "hamburg"], $provider->getConfig());
    }

    public function testWithHeader()
    {
        // GIVEN
        $provider = new ExcelProvider();
        $provider->configure(['sourceFile' => __DIR__ . '/test.xlsx', 'excelVersion' => 'Excel2007', 'header' => true]);
        $iterator = $provider->getIterator();

        // WHEN
        $iterator->rewind();

        // THEN
        $this->assertEquals('a', $iterator->current()['h1']);
        $this->assertEquals('b', $iterator->current()['h2']);
        $this->assertEquals('c', $iterator->current()['']);
        $this->assertEquals('d', $iterator->current()['h3']);

        // WHEN
        $iterator->next();

        // THEN
        $this->assertEquals('x', $iterator->current()['h1']);
        $this->assertEquals('sss', $iterator->current()['h2']);
        $this->assertEquals('', $iterator->current()['']);
        $this->assertEquals('', $iterator->current()['h3']);
    }

    public function testNoHeader()
    {
        // GIVEN
        $provider = new ExcelProvider();
        $provider->configure(['sourceFile' => __DIR__ . '/test.xlsx', 'excelVersion' => 'Excel2007', 'header' => false]);
        $iterator = $provider->getIterator();

        // WHEN
        $iterator->rewind();

        // THEN
        $this->assertEquals('h1', $iterator->current()[0]);
        $this->assertEquals('h2', $iterator->current()[1]);
        $this->assertEquals('', $iterator->current()[2]);
        $this->assertEquals('h3', $iterator->current()[3]);

        // WHEN
        $iterator->next();

        // THEN
        $this->assertEquals('a', $iterator->current()[0]);
        $this->assertEquals('b', $iterator->current()[1]);
        $this->assertEquals('c', $iterator->current()[2]);
        $this->assertEquals('d', $iterator->current()[3]);

        // WHEN
        $iterator->next();

        // THEN
        $this->assertEquals('x', $iterator->current()[0]);
        $this->assertEquals('sss', $iterator->current()[1]);
        $this->assertEquals('', $iterator->current()[2]);
        $this->assertEquals('', $iterator->current()[3]);
    }
}
