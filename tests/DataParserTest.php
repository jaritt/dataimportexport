<?php
/**
 * Created by PhpStorm.
 * User: felix.welter
 * Date: 20.07.2017
 * Time: 10:11
 */

use Mate\DataImportExport\CSVProvider;
use Mate\DataImportExport\DataImportExport;
use Mate\DataImportExport\ExcelProvider;
use Orchestra\Testbench\TestCase;

class DataParserTest extends TestCase
{
    public function testGiveCsvProvider()
    {
        $this->assertInstanceOf(CSVProvider::class, DataImportExport::getCSVProvider());
    }
    public function testGiveExcelProvider()
    {
        $this->assertInstanceOf(ExcelProvider::class, DataImportExport::getExcelProvider());
    }
}
