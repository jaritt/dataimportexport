<?php

namespace Mate\DataImportExport;

class DataImportExport
{
    static public function getCSVProvider($conf = [])
    {
        return new CSVProvider(array_merge([
            'delimiter' => ';',
            'header' => true
        ], $conf));
    }

    static public function getExcelProvider($conf = [])
    {
        return new ExcelProvider(array_merge([
            'excelVersion' => 'Excel2007',
            'header' => true
        ], $conf));
    }
}