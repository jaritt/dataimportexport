<?php
/**
 * Created by PhpStorm.
 * User: felix.welter
 * Date: 19.07.2017
 * Time: 16:12
 */

namespace Mate\DataImportExport;


class ExcelIterator implements \Iterator
{
    protected $currentRow;
    protected $current;
    protected $objWorksheet;
    protected $headers;
    protected $lastColumn;
    protected $lastRow;

    function __construct(\PHPExcel_Worksheet $worksheet, $headers = null)
    {
        $this->headers = $headers;
        $this->currentRow = $this->firstRow();
        $this->objWorksheet = $worksheet;
        $this->lastColumn = $this->objWorksheet->getHighestColumn();
        $this->lastRow = $this->objWorksheet->getHighestRow();
    }

    protected function firstRow()
    {
        return $this->headers ? 2 : 1;
    }

    public function rewind()
    {
        $this->currentRow = $this->firstRow();
        $this->current = null;
    }

    public function next()
    {
        ++$this->currentRow;
    }

    public function key()
    {
        return $this->currentRow;
    }

    public function current()
    {
        $target = 'A' . $this->currentRow . ':' . $this->lastColumn . $this->currentRow;
        if ($this->headers) {
            return array_combine($this->headers, $this->objWorksheet->rangeToArray($target)[0]);
        }
        return $out = $this->objWorksheet->rangeToArray($target)[0];
    }

    public function valid()
    {
        return $this->lastRow >= $this->currentRow;
    }
}