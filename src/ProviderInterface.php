<?php
/**
 * Created by PhpStorm.
 * User: felix.welter
 * Date: 19.07.2017
 * Time: 11:44
 */

namespace Mate\DataImportExport;


interface ProviderInterface
{
    public function setConfig($conf);
    public function configure($conf);
    public function getConfig();
    public function execute();
    public function getIterator();
    public function getBaseLibraryObject();
}