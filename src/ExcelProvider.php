<?php

namespace Mate\DataImportExport;

/**
 * Created by PhpStorm.
 * User: felix.welter
 * Date: 19.07.2017
 * Time: 14:03
 */

class ExcelProvider extends BasicProvider
{
    public function internalExecute()
    {
        $conf = $this->getConfig();

        $objReader = \PHPExcel_IOFactory::createReader($conf['excelVersion']);
        $objReader->setReadDataOnly(TRUE);
        $objPHPExcel = $objReader->load($conf['sourceFile']);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $header = false;
        if (isset($conf['header']) && $conf['header']) {
            $header = $objWorksheet->rangeToArray('A1:' . $objWorksheet->getHighestColumn() . '1')[0];
        }

        $this->iterator = new ExcelIterator($objWorksheet, $header);
        $this->baseLibraryObject = $objWorksheet;
        $this->executed = true;
    }
}