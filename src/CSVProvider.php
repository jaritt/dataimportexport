<?php
/**
 * Created by PhpStorm.
 * User: felix.welter
 * Date: 19.07.2017
 * Time: 12:00
 */

namespace Mate\DataImportExport;

use League\Csv\Reader;

class CSVProvider extends BasicProvider
{
    public function internalExecute()
    {
        $conf = $this->getConfig();
        $csv = Reader::createFromPath($conf['sourceFile']);
        $csv->setDelimiter($conf['delimiter']);
        $this->iterator = $conf['header'] ? $csv->fetchAssoc() : $csv->fetch();
        $this->baseLibraryObject = $csv;
        $this->executed = true;
    }
}