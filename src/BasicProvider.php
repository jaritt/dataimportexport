<?php
/**
 * Created by PhpStorm.
 * User: felix.welter
 * Date: 19.07.2017
 * Time: 11:45
 */

namespace Mate\DataImportExport;

abstract class BasicProvider implements ProviderInterface
{
    public $config;
    protected $baseLibraryObject;
    protected $executed;
    protected $iterator;

    function __construct($conf = [])
    {
        $this->setConfig($conf);
    }

    public function setConfig($conf)
    {
        $this->config = $conf;
        $this->executed = false;
    }

    public function configure($conf)
    {
        $this->setConfig(array_merge($this->config, $conf));
    }

    public function getConfig()
    {
        return $this->config;
    }

    public function execute()
    {
        $this->executed || $this->internalExecute();
        return $this;
    }

    public function reload()
    {
        $this->executed = false;
        $this->execute();
    }

    public function getIterator()
    {
        $this->execute();
        return $this->iterator;
    }

    public function getBaseLibraryObject()
    {
        $this->execute();
        return $this->baseLibraryObject;
    }
}