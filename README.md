# DataImportExport
Simple library to quickly load data from CSV or Excel files.

## CSV
```php
$provider = DataImportExport::getCSVProvider();
$provider->configure([
    'sourceFile' => '/path/to/file.csv', 
    'delimiter' => ',',
    'header' => 'true'
]);
$iterator = $provider->getIterator();
```

## Excel
```php
$provider = DataImportExport::getExcelProvider();
$provider->configure([
    'sourceFile' => '/path/to/file.xlsx', 
    'excelVersion' => 'Excel2007', 
    'header' => true
]);
$iterator = $provider->getIterator();
```